# Demos



## Extracting text from pdf

Demos for the following Python libraries are available.

* [PyPDF2](https://github.com/mstamy2/PyPDF2)
  A demo of PyPDF2 for a few pdf documents can be found at [PyPDF2.ipynb](PyPDF2.ipynb).
  **Issues:** May not be able to decode or decrypt some pdf documents, and sometimes identifies characters incorrectly.


* [PDFMiner](https://github.com/pdfminer/pdfminer.six)
  A demo of PDFMiner.six for a few pdf documents can be found at [PDFMiner.ipynb](PDFMiner.ipynb).
  **Issues:** Texts may not always be in the expected order, especially in case of multiple columns and tables.



## Extracting table from pdf using [tabula-py](https://github.com/chezou/tabula-py)

A demo for extracting tables from a pdf document using tabula-py can be found at [tabula.ipynb](tabula.ipynb).
**Issues:** Need to specify where the table is.



## Authors
* Stanley Simoes

