#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Sep  4 18:39:03 2020

@author: simoes
"""


# %% Libraries
import tabula
from nltk.tokenize import word_tokenize
from typing import Dict, List, Tuple


# %% Extract texts from all pages
def extract_text(path: str,
                 pages='all',
                 top: float = 0,
                 left: float = 0,
                 bottom: float = 100,
                 right: float = 100
                 ) -> List[List[str]]:
    dfs = tabula.read_pdf(
        path, pages=pages,
        area=[top, left, bottom, right],
        relative_area=True,
        multiple_tables=True, stream=True, guess=False,
        )
    # concatenate texts
    pages = [
        df.fillna('').apply(' '.join, axis=1).tolist()
        for df in dfs
        ]
    return pages


# %% Rank pages on number of mandatory/preferred keywords
def rank_pages(pages: List[List[str]],
               mandatory: List[str],
               preferred: List[str]
               ) -> Dict[int, int]:
    # lower case
    mandatory = set(map(str.lower, mandatory))
    preferred = set(map(str.lower, preferred))
    # get tokens in each page
    tokens = [
        set(word_tokenize('\n'.join(page).lower()))
        for page in pages
        ]
    # search for pages with all mandatory keywords
    relevant = {
        i: p
        for i, p in enumerate(tokens)
        if mandatory.issubset(p)
        }
    # rank pages on number of unique preferable keywords
    scores = {
        i: len(preferred.intersection(p))
        for i, p in relevant.items()
        }
    rank = {
        i: s
        for i, s in sorted(scores.items(), key=lambda it: it[1], reverse=True)
        }
    return rank


# %% Align two columns in a page
def align_columns(page: List[str],
                  col1_u: List[str],
                  col2_u: List[str]
                  ) -> Tuple[List[str], List[str]]:
    assert len(col1_u) <= len(page)
    assert len(col2_u) <= len(page)
    # append delimiters to indicate end
    col1_u.append('_EOC1_')
    col2_u.append('_EOC2_')
    page.append('_EOC1_ _EOC2_')
    i1, col1_a = 0, []
    i2, col2_a = 0, []
    for line in page:
        line_s = ''.join(line.split())      # remove all whitespace
        # text starts with column 1?
        c1_s = ''.join(col1_u[i1].split())  # remove all whitespace
        if line_s.startswith(c1_s):
            col1_a.append(col1_u[i1])
            i1 += 1
        else:
            col1_a.append('')
        # text ends with column 2?
        c2_s = ''.join(col2_u[i2].split())  # remove all whitespace
        if line_s.endswith(c2_s):
            col2_a.append(col2_u[i2])
            i2 += 1
        else:
            col2_a.append('')
        # text must start with col1_u or end with col2_u or both
        if col1_a[-1] == '' and col2_a[-1] == '':
            _ = col1_u.pop(), col2_u.pop(), page.pop()
            raise ValueError
    assert col1_a[-1] == '_EOC1_'
    assert col2_a[-1] == '_EOC2_'
    # remove delimiters
    _ = col1_u.pop(), col2_u.pop(), page.pop()
    _ = col1_a.pop(), col2_a.pop()
    assert len(page) == len(col1_a) == len(col2_a)
    return col1_a, col2_a


# %% Get indices of lines containing keywords
def get_kwd_indices(lines: List[str],
                    keywords: List[str]
                    ) -> Tuple[int, int]:
    # lower case and tokenize
    lower = map(str.lower, lines)
    tokens = list(map(word_tokenize, lower))
    # find keyword indices
    scores = dict()
    for i, line in enumerate(tokens):
        n = len(set(keywords).intersection(line))
        if n > 0:
            scores[i] = n
    # failure if no keywords found
    if len(scores) == 0:
        raise KeyError('No keywords found')
    # gather consecutive lines
    gathered = [[]]
    for i in range(max(scores)+1):
        if i in scores:
            gathered[-1].append(i)      # append to last list
        elif len(gathered[-1]) != 0:
            gathered.append([])         # create new list
    # sum up counts of consecutive lines
    summed = {(min(g), max(g)): sum(map(scores.get, g))
              for g in gathered}
    # get indices of lines with max sum of counts
    start, end = sorted(summed.items(), key=lambda item: item[1])[-1][0]
    return start, end


# %% Merge lines into a 2-column row
def make_row(col1: List[str],
             col2: List[str],
             start: int,
             end: int
             ) -> Tuple[str, str]:
    while col1[end+1].strip() == '' and col2[end+1].strip() != '':
        end += 1
    col1_s = map(str.strip, col1[start: end+1])
    col2_s = map(str.strip, col2[start: end+1])
    row = ' '.join(col1_s).strip(), ' '.join(col2_s).strip()
    return row


# %% Extract row from 2-column table with keywords in the first column
def extract_row_kwd(path: str,
                    pages: List[List[str]],
                    mandatory: List[str],
                    preferred: List[str],
                    keywords: List[str]
                    ):
    # identify best page with the mandatory and preferred keywords
    best = list(rank_pages(pages, mandatory, preferred).items())[0][0]
    # extract the two columns from the best page
    # +1 because page indices start from 1 for tabula
    col1 = extract_text(path, pages=best+1, right=50)[0]
    col2 = extract_text(path, pages=best+1, left=50)[0]
    try:
        # align the two columns
        col1, col2 = align_columns(pages[best], col1, col2)
    except ValueError:
        raise ValueError(f'Unable to align texts in page {best} of {path}')
    # get indices of lines containing the keywords in column 1
    start, end = get_kwd_indices(col1, keywords)
    # merge lines to make a row
    item1, item2 = make_row(col1, col2, start, end)
    return best, item1, item2


# %%
