#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Sep  9 23:17:00 2020

@author: simoes
"""


# %% Libraries
import os
import pandas
import scipy.sparse
from nltk.tokenize import word_tokenize
from sklearn.feature_extraction.text import TfidfVectorizer
from sklearn.linear_model import LogisticRegression
from sklearn.utils.class_weight import compute_sample_weight
from typing import Dict, List

from utils import extract_text


# %% Paths
PAGE_PATH = '../resources/contents_page.tsv'
PDF_DIR = '../resources/files/roi sample/'


# %% Get important tokens
def rank_tokens(docs: Dict[str, List[str]],
                labels: pandas.Series
                ) -> Dict[str, float]:
    # fit vectorizer
    print('Creating vectorizer..')
    vectorizer = TfidfVectorizer(
        lowercase=True, tokenizer=str.split, token_pattern=None)
    _ = vectorizer.fit(page for pages in docs.values() for page in pages)
    inverse_vocabulary = dict(
        zip(vectorizer.vocabulary_.values(), vectorizer.vocabulary_.keys()))
    # prepare training data
    print('Preparing training data..')
    X = {f: vectorizer.transform(pages) for f, pages in docs.items()}
    y = dict()
    for f, x in X.items():
        lbl = list(map(int, labels[f].split(',')))
        y[f] = [1 if i in lbl else 0 for i in range(x.shape[0])]
    # stack
    X_stacked = scipy.sparse.vstack(X.values())
    y_stacked = [j for i in y.values() for j in i]
    # train classifier
    print('Training classifier..')
    clsfr = LogisticRegression(solver='liblinear')
    sample_weight = compute_sample_weight(
        class_weight='balanced', y=y_stacked)
    _ = clsfr.fit(
        X=X_stacked, y=y_stacked, sample_weight=sample_weight)
    accuracy = clsfr.score(
        X=X_stacked, y=y_stacked, sample_weight=sample_weight)
    print(f'  classifier accuracy: {accuracy}')
    # extract classifier weights
    weights = {v: clsfr.coef_[0][k] for k, v in inverse_vocabulary.items()}
    # rank in non-increasing order
    weights = {
        k: v
        for k, v in sorted(weights.items(), key=lambda it: it[1], reverse=True)
        }
    return weights


# %% main
if __name__ == '__main__':
    # read labelled data
    print('Reading labelled data..')
    labels = pandas.read_csv(PAGE_PATH, sep='\t', index_col='file', dtype=str)

    # extract texts from pdf documents
    print('Extracting texts..')
    docs = dict()
    for f in labels.index:
        try:
            docs[f] = extract_text(os.path.join(PDF_DIR, f))
        except FileNotFoundError as e:
            print(e)

    # merge lines in each page
    merged = {
        f: [
            ' '.join(word for line in page for word in word_tokenize(line))
            for page in pages
            ]
        for f, pages in docs.items()
        }

    # get important words
    weights = dict()
    print('\ncontents.fire')
    weights['fire'] = rank_tokens(docs=merged, labels=labels['fire'])
    print('\ncontents.smoke')
    weights['smoke'] = rank_tokens(docs=merged, labels=labels['smoke'])
    print('\ncontents.storm')
    weights['storm or flood'] = rank_tokens(docs=merged, labels=labels['storm or flood'])
    print('\ncontents.water')
    weights['escape of water'] = rank_tokens(docs=merged, labels=labels['escape of water'])
    print('\ncontents.stealing')
    weights['stealing'] = rank_tokens(docs=merged, labels=labels['stealing'])
    print('\ncontents.oil')
    weights['escape of oil'] = rank_tokens(docs=merged, labels=labels['escape of oil'])