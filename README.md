# MapCover IE

Repository for the Map Cover project.


## Prerequisites
* Java 8+ (required for the [tabula-py](https://github.com/chezou/tabula-py) library)
* [Anaconda](https://www.anaconda.com/)


## Getting Started
Create a conda environment using [environment.yaml](./environment.yaml)
```bash
conda env create -f environment.yaml
conda activate mapcover
```


## Demos
Demos can be found in the [demo](./demo/) directory.


## Authors
* Stanley Simoes

