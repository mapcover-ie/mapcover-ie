#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Sep  7 02:58:29 2020

@author: simoes
"""


# %% Libraries
import os
from typing import Dict, List, Tuple
from ruamel import yaml
from utils import extract_text, extract_row_kwd


# %% Paths
PDF_DIR = '../resources/files/roi sample/'
RULES_PATH = '../resources/rules.yaml'


# %%
def extract_contents(docs: Dict[str, List[List[str]]],
                     rules: Dict[str, List[str]]
                     ) -> Dict[str, Tuple[int, str, str]]:
    contents = dict()
    for f, pages in docs.items():
        path = os.path.join(PDF_DIR, f)
        try:
            contents[f] = extract_row_kwd(
                path, pages,
                rules['mandatory'], rules['preferred'],
                rules['keywords']
                )
        except ValueError as e:
            print(e, '\n  Skipping this pdf')
    return contents


# %% main
if __name__ == '__main__':
    # get list of pdfs
    print('Listing pdfs..', end=' ')
    pdfs = [f for f in os.listdir(PDF_DIR) if f.endswith('.pdf')]
    print(f'{len(pdfs)} found')
    # extract texts
    print('Extracting texts..')
    docs = {
        f: extract_text(os.path.join(PDF_DIR, f))
        for f in pdfs
        }

    # read extraction rules from yaml file
    print('Loading extraction rules..')
    with open(RULES_PATH) as f:
        rules = yaml.round_trip_load(f)

    # extract contents.fire
    print('Extracting contents.fire')
    contents_fire = extract_contents(docs, rules['contents']['fire'])
    print()

    # extract contents.smoke
    print('Extracting contents.smoke')
    contents_smoke = extract_contents(docs, rules['contents']['smoke'])
    print()

    # extract contents.storm
    print('Extracting contents.storm')
    contents_storm = extract_contents(docs, rules['contents']['storm'])
    print()


# %%